volatile long speed_counter = 0;

void increment_speed_counter()
{
    speed_counter++;
}
END_OF_FUNCTION(increment_speed_counter);

class image
{
    public:
    int width, height, x, y;
    BITMAP *img;
};

void initializeCrap()
{
    allegro_init();
    install_keyboard();
    install_timer();
    install_mouse();

    LOCK_VARIABLE(speed_counter);
    LOCK_FUNCTION(increment_speed_counter);
    install_int_ex(increment_speed_counter,BPS_TO_TIMER(120));
    install_sound(DIGI_AUTODETECT, MIDI_AUTODETECT, 0);

    set_color_depth(16);
    set_gfx_mode(GFX_AUTODETECT_WINDOWED, 800, 600, 0, 0);
}
END_OF_FUNCTION(initializeCrap);

void initializeImages(image *btnFullscreen, image *btnExit, image *btnWindow, image *btnPlay, image *btnHelp, image *windowHelp, image *btnBack) {
    btnFullscreen->img = load_bitmap("imgOptionFullscreen.bmp", NULL);
    btnFullscreen->width = btnFullscreen->img->w;
    btnFullscreen->height = btnFullscreen->img->h;
    btnFullscreen->x = 400 - (btnFullscreen->width / 2);
    btnFullscreen->y = 330;

    btnExit->img = load_bitmap("imgOptionExit.bmp", NULL);
    btnExit->width = btnExit->img->w;
    btnExit->height = btnExit->img->h;
    btnExit->x = 400 - (btnExit->width / 2);
    btnExit->y = 370;

    btnHelp->img = load_bitmap("imgOptionHelp.bmp", NULL);
    btnHelp->width = btnHelp->img->w;
    btnHelp->height = btnHelp->img->h;
    btnHelp->x = 400 - (btnHelp->width / 2);
    btnHelp->y = 290;

    btnWindow->img = load_bitmap("imgOptionWindow.bmp", NULL);
    btnWindow->width = btnWindow->img->w;
    btnWindow->height = btnWindow->img->h;
    btnWindow->x = 400 - (btnWindow->width / 2);
    btnWindow->y = 330;

    btnPlay->img = load_bitmap("imgOptionPlay.bmp",NULL);
    btnPlay->width = btnPlay->img->w;
    btnPlay->height = btnPlay->img->h;
    btnPlay->x = 400 - (btnPlay->width / 2);
    btnPlay->y = 250;

    windowHelp->img = load_bitmap("imgWindowHelp.bmp", NULL);
    windowHelp->width = windowHelp->img->w;
    windowHelp->height = windowHelp->img->h;
    windowHelp->x = 400 - (windowHelp->width / 2);
    windowHelp->y = 300 - (windowHelp->height / 2);

    btnBack->img = load_bitmap("imgOptionBack.bmp", NULL);
    btnBack->width = windowHelp->img->w;
    btnBack->height = windowHelp->img->h;
    btnBack->x = 590;
    btnBack->y = 440;
}
END_OF_FUNCTION(initializeImages);

void displayImage(image *theimage, BITMAP *buffer) {
    draw_sprite(buffer, theimage->img, theimage->x, theimage->y);
}
END_OF_FUNCTION(displayImage);

bool checkClicking(int mouse_x, int mouse_y, image *button) {
    if (    (mouse_x >= button->x) && (mouse_x <= (button->x + button->width))
        &&  (mouse_y >= button->y) && (mouse_y <= (button->y + button->height)))
    {
        return true;
    }
    return false;
}
END_OF_FUNCTION(checkClicking);

class player
{
    public:
    int x, y, collideWidth, collideHeight, totalDeaths, totalKills, spriteWidth, spriteHeight;
    char direction;
    BITMAP *img;
    bool shot;
    char shotDir;
    int shotX, shotY;
    player()
    {
        shot = false;
        totalDeaths = 0;
        totalKills = 0;
        direction = 'l';
    }
};

void initializePlayerCrap(player *player1, player *player2, player *player3, player *player4)
{
    player1->x = 10; player1->y = 100;  player1->img = load_bitmap("imgChickenSet.bmp", NULL);
    player1->spriteWidth = 28; player1->spriteHeight = 26; player1->direction = 'r';
    player2->x = 10; player2->y = 500;  player2->img = load_bitmap("imgCatSet.bmp", NULL);
    player2->spriteWidth = 32; player2->spriteHeight = 28; player2->direction = 'r';
    player3->x = 750; player3->y = 100; player3->img = load_bitmap("imgBoatSet.bmp", NULL);
    player3->spriteWidth = 24; player3->spriteHeight = 16;
    player4->x = 750; player4->y = 500; player4->img = load_bitmap("imgSquashySet.bmp", NULL);
    player4->spriteWidth = 35; player4->spriteHeight = 35;
}
END_OF_FUNCTION(initializePlayerCrap);

bool checkCollision (player *playerlaser, player *playertarget)
{
    if ((   (playerlaser->shotY + 10 > playertarget->y) &&
            (playerlaser->shotY < playertarget->y + playertarget->spriteHeight) &&
            (playerlaser->shotX + 25 > playertarget->x) &&
            (playerlaser->shotX < playertarget->x + playertarget->spriteWidth)))
        {
            return true;
        }
    return false;
}
END_OF_FUNCTION(checkCollision);


