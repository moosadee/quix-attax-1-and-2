#include <allegro.h>
#include <cstdlib>
#include <ctime>
#include "Extra.h"

/*      QUIX ATTAX
    All programmed in one day
    Nov 30, 2006
    A game I made quickly,
    to be a simple game,
    thus "quix" = quick
    programming time and
    quick amount of time
    you'll be interested
    in this game.

    Like two minutes. :P

    Made with C++
    (Code::Blocks)
    and Allegro
*/

int main()
{
    initializeCrap();

    bool menu = true;
    BITMAP *buffer = create_bitmap(800,600);
    BITMAP *imgTitle = load_bitmap("imgTitle.bmp", NULL);
    BITMAP *imgMenuBox = load_bitmap("imgMenuBox.bmp", NULL);
    BITMAP *cursor = load_bitmap("imgCow.bmp", NULL);
    BITMAP *lasers = load_bitmap ("imgLasers.bmp", NULL);
    BITMAP *imgBackground1 = load_bitmap("imgBackgroundMenu.bmp", NULL);
    BITMAP *imgBackground2 = load_bitmap("imgBackground.bmp", NULL);
    BITMAP *imgScoreList = load_bitmap("imgScoreList.bmp", NULL);
    BITMAP *imgBlackBar = load_bitmap("blackbar.bmp", NULL);
    bool fullscreen = false;

    SAMPLE *soundCat = load_sample("Cat.wav");
    SAMPLE *soundBoat = load_sample("Boat.wav");
    SAMPLE *soundSquashy = load_sample("Squashy.wav");
    SAMPLE *soundChicken = load_sample("Chicken.wav");
    int pan = 128;
    int pitch = 1000;

    int shotSpeed = 5;

    //allegro_message("HI");        this is like a message box, whee!

    image btnFullscreen, btnExit, btnWindow, btnPlay, btnHelp, windowHelp, btnBack;
    player player1, player2, player3, player4;
    initializeImages(&btnFullscreen, &btnExit, &btnWindow, &btnPlay, &btnHelp, &windowHelp, &btnBack);
    initializePlayerCrap(&player1, &player2, &player3, &player4);

    bool mouseClick = false;
    bool helpwindow = false;
    bool done = false;
    float frame = 1.0;

    bool allowDeaths = true;

    while (!done)
    {
        draw_sprite(buffer, imgBackground2, 0, 0);
        while (speed_counter > 0)
        {
            if (menu == true && helpwindow == true)
            {
                if (mouse_b & 1)
                {
                    mouseClick = true;
                    if (checkClicking(mouse_x, mouse_y, &btnBack)) { helpwindow = false; }
                }
                else { mouseClick = false; }
            }
            else if (menu == true && helpwindow == false)
            {
                //Mouse button clicked
                if (mouse_b & 1)
                {
                    mouseClick = true;
                    //Check to see if user is clicking a button
                    if (checkClicking(mouse_x, mouse_y, &btnHelp)) { helpwindow = true; }
                    if (checkClicking(mouse_x, mouse_y, &btnExit)) { done = true; }
                    if (!fullscreen)
                    {
                        if (checkClicking(mouse_x, mouse_y, &btnFullscreen)) { set_gfx_mode(GFX_AUTODETECT_FULLSCREEN, 800, 600, 0, 0); fullscreen = true; }
                    }
                    else
                    {
                        if (checkClicking(mouse_x, mouse_y, &btnWindow)) { set_gfx_mode(GFX_AUTODETECT_WINDOWED, 800, 600, 0, 0); fullscreen = false; }
                    }
                    if (checkClicking(mouse_x, mouse_y, &btnPlay)) { menu = false; }
                }
                else { mouseClick = false; }
            } //if (menu == true)
            else//Menu isn't open-- normal gameplay
            {
                if (key[KEY_ESC]) { menu = true; }
                if (key[KEY_Z]) { done = true; }
                //PLAYER 1
                if (key[KEY_W])     { if (player1.y >= 0)   { player1.y--; player1.direction = 'u'; } }
                if (key[KEY_A])     { if (player1.x >= 0)   { player1.x--; player1.direction = 'l'; } }
                if (key[KEY_S])     { if (player1.y <= 575) { player1.y++; player1.direction = 'd'; } }
                if (key[KEY_D])     { if (player1.x <= 775) { player1.x++; player1.direction = 'r'; } }
                //PLAYER 2
                if (key[KEY_I])     { if (player2.y >= 0)   { player2.y--; player2.direction = 'u'; } }
                if (key[KEY_J])     { if (player2.x >= 0)   { player2.x--; player2.direction = 'l'; } }
                if (key[KEY_K])     { if (player2.y <= 575) { player2.y++; player2.direction = 'd'; } }
                if (key[KEY_L])     { if (player2.x <= 775) { player2.x++; player2.direction = 'r'; } }
                //PLAYER 3
                if (key[KEY_UP])    { if (player3.y >= 0)   { player3.y--; } player3.direction = 'u'; }
                if (key[KEY_LEFT])  { if (player3.x >= 0)   { player3.x--; } player3.direction = 'l'; }
                if (key[KEY_DOWN])  { if (player3.y <= 575) { player3.y++; } player3.direction = 'd'; }
                if (key[KEY_RIGHT]) { if (player3.x <= 775) { player3.x++; } player3.direction = 'r'; }
                //PLAYER 4
                if (key[KEY_8_PAD]) { if (player4.y >= 0)   { player4.y--; } player4.direction = 'u'; }
                if (key[KEY_4_PAD]) { if (player4.x >= 0)   { player4.x--; } player4.direction = 'l'; }
                if (key[KEY_2_PAD]) { if (player4.y <= 575) { player4.y++; } player4.direction = 'd'; }
                if (key[KEY_6_PAD]) { if (player4.x <= 775) { player4.x++; } player4.direction = 'r'; }


                frame += 0.1;
                //Shooting
                if (player1.shot == false) { player1.shot = true; player1.shotDir = player1.direction; player1.shotX = player1.x; player1.shotY = player1.y + 5; }
                else
                {
                   if       (player1.shotDir == 'l') { player1.shotX -= shotSpeed; }
                   else if  (player1.shotDir == 'r') { player1.shotX += shotSpeed; }
                   else if  (player1.shotDir == 'u') { player1.shotY -= shotSpeed; }
                   else if  (player1.shotDir == 'd') { player1.shotY += shotSpeed; }
                   //If bullet goes off the screen, destroy it so you can shoot again.
                   if (player1.shotX < 0 || player1.shotX > 800 || player1.shotY < 0 || player1.shotY > 600) { player1.shot = false; }
                }
                if (player2.shot == false) { player2.shot = true; player2.shotDir = player2.direction; player2.shotX = player2.x; player2.shotY = player2.y + 5; }
                else
                {
                   if       (player2.shotDir == 'l') { player2.shotX -= shotSpeed; }
                   else if  (player2.shotDir == 'r') { player2.shotX += shotSpeed; }
                   else if  (player2.shotDir == 'u') { player2.shotY -= shotSpeed; }
                   else if  (player2.shotDir == 'd') { player2.shotY += shotSpeed; }
                   //If bullet goes off the screen, destroy it so you can shoot again.
                   if (player2.shotX < 0 || player2.shotX > 800 || player2.shotY < 0 || player2.shotY > 600) { player2.shot = false; }
                }
                if (player3.shot == false) { player3.shot = true; player3.shotDir = player3.direction; player3.shotX = player3.x; player3.shotY = player3.y + 5; }
                else
                {
                   if       (player3.shotDir == 'l') { player3.shotX -= shotSpeed; }
                   else if  (player3.shotDir == 'r') { player3.shotX += shotSpeed; }
                   else if  (player3.shotDir == 'u') { player3.shotY -= shotSpeed; }
                   else if  (player3.shotDir == 'd') { player3.shotY += shotSpeed; }
                   //If bullet goes off the screen, destroy it so you can shoot again.
                   if (player3.shotX < 0 || player3.shotX > 800 || player3.shotY < 0 || player3.shotY > 600) { player3.shot = false; }
                }
                if (player4.shot == false) { player4.shot = true; player4.shotDir = player4.direction; player4.shotX = player4.x; player4.shotY = player4.y + 5; }
                else
                {
                   if       (player4.shotDir == 'l') { player4.shotX -= shotSpeed; }
                   else if  (player4.shotDir == 'r') { player4.shotX += shotSpeed; }
                   else if  (player4.shotDir == 'u') { player4.shotY -= shotSpeed; }
                   else if  (player4.shotDir == 'd') { player4.shotY += shotSpeed; }
                   //If bullet goes off the screen, destroy it so you can shoot again.
                   if (player4.shotX < 0 || player4.shotX > 800 || player4.shotY < 0 || player4.shotY > 600) { player4.shot = false; }
                }
// CHECK DEATHS - lol, way too big.
                if (allowDeaths == true)
                {
                    if (checkCollision(&player1, &player2))
                    {
                        //Play noise
                        play_sample(soundCat, 255, pan, pitch, FALSE); //sample, volume, pan, frequency, loop
                        //P2 was killed
                        player1.totalKills++; player2.totalDeaths++;
                        //Put P2 at new coordinates.
                        srand((unsigned)time(NULL)+4); int temprand = (int)775 * rand() / (RAND_MAX + 1.0); player2.x = temprand; temprand = (int)575 * rand() / (RAND_MAX + 1.0); player2.y = temprand;
                        player1.shot = false;
                    }

                    if (checkCollision(&player1, &player3))
                    {
                        //Play noise
                        play_sample(soundBoat, 255, pan, pitch, FALSE); //sample, volume, pan, frequency, loop
                        //P2 was killed
                        player1.totalKills++; player3.totalDeaths++;
                        //Put P2 at new coordinates.
                        srand((unsigned)time(NULL)+2); int temprand = (int)775 * rand() / (RAND_MAX + 1.0); player3.x = temprand; temprand = (int)575 * rand() / (RAND_MAX + 1.0); player3.y = temprand;
                        player1.shot = false;
                    }

                    if (checkCollision(&player1, &player4))
                    {
                        //Play noise
                        play_sample(soundSquashy, 255, pan, pitch, FALSE); //sample, volume, pan, frequency, loop
                        //P2 was killed
                        player1.totalKills++; player4.totalDeaths++;
                        //Put P2 at new coordinates.
                        srand((unsigned)time(NULL)+10); int temprand = (int)775 * rand() / (RAND_MAX + 1.0); player4.x = temprand; temprand = (int)575 * rand() / (RAND_MAX + 1.0); player4.y = temprand;
                        player1.shot = false;
                    }


                    if (checkCollision(&player2, &player1))
                    {
                        //Play noise
                        play_sample(soundChicken, 255, pan, pitch, FALSE); //sample, volume, pan, frequency, loop
                        //P2 was killed
                        player2.totalKills++; player1.totalDeaths++;
                        //Put P2 at new coordinates.
                        srand((unsigned)time(NULL)+8); int temprand = (int)775 * rand() / (RAND_MAX + 1.0); player1.x = temprand; temprand = (int)575 * rand() / (RAND_MAX + 1.0); player1.y = temprand;
                        player2.shot = false;
                    }

                    if (checkCollision(&player2, &player3))
                    {
                        //Play noise
                        play_sample(soundBoat, 255, pan, pitch, FALSE); //sample, volume, pan, frequency, loop
                        //P2 was killed
                        player2.totalKills++; player3.totalDeaths++;
                        //Put P2 at new coordinates.
                        srand((unsigned)time(NULL)+20); int temprand = (int)775 * rand() / (RAND_MAX + 1.0); player3.x = temprand; temprand = (int)575 * rand() / (RAND_MAX + 1.0); player3.y = temprand;
                        player2.shot = false;
                    }

                    if (checkCollision(&player2, &player4))
                    {
                        //Play noise
                        play_sample(soundSquashy, 255, pan, pitch, FALSE); //sample, volume, pan, frequency, loop
                        //P2 was killed
                        player2.totalKills++; player4.totalDeaths++;
                        //Put P2 at new coordinates.
                        srand((unsigned)time(NULL)+3); int temprand = (int)775 * rand() / (RAND_MAX + 1.0); player4.x = temprand; temprand = (int)575 * rand() / (RAND_MAX + 1.0); player4.y = temprand;
                        player2.shot = false;
                    }

                    if (checkCollision(&player3, &player1))
                    {
                        //Play noise
                        play_sample(soundChicken, 255, pan, pitch, FALSE); //sample, volume, pan, frequency, loop
                        //P2 was killed
                        player3.totalKills++; player1.totalDeaths++;
                        //Put P2 at new coordinates.
                        srand((unsigned)time(NULL)+5); int temprand = (int)775 * rand() / (RAND_MAX + 1.0); player1.x = temprand; temprand = (int)575 * rand() / (RAND_MAX + 1.0); player1.y = temprand;
                        player3.shot = false;
                    }

                    if (checkCollision(&player3, &player2))
                    {
                        //Play noise
                        play_sample(soundCat, 255, pan, pitch, FALSE); //sample, volume, pan, frequency, loop
                        //P2 was killed
                        player3.totalKills++; player2.totalDeaths++;
                        //Put P2 at new coordinates.
                        srand((unsigned)time(NULL)+7); int temprand = (int)775 * rand() / (RAND_MAX + 1.0); player2.x = temprand; temprand = (int)575 * rand() / (RAND_MAX + 1.0); player2.y = temprand;
                        player3.shot = false;
                    }

                    if (checkCollision(&player3, &player4))
                    {
                        //Play noise
                        play_sample(soundSquashy, 255, pan, pitch, FALSE); //sample, volume, pan, frequency, loop
                        //P2 was killed
                        player3.totalKills++; player4.totalDeaths++;
                        //Put P2 at new coordinates.
                        srand((unsigned)time(NULL)+9); int temprand = (int)775 * rand() / (RAND_MAX + 1.0); player4.x = temprand; temprand = (int)575 * rand() / (RAND_MAX + 1.0); player4.y = temprand;
                        player3.shot = false;
                    }


                    if (checkCollision(&player4, &player1))
                    {
                        //Play noise
                        play_sample(soundChicken, 255, pan, pitch, FALSE); //sample, volume, pan, frequency, loop
                        //P2 was killed
                        player4.totalKills++; player1.totalDeaths++;
                        //Put P2 at new coordinates.
                        srand((unsigned)time(NULL)+11); int temprand = (int)775 * rand() / (RAND_MAX + 1.0); player1.x = temprand; temprand = (int)575 * rand() / (RAND_MAX + 1.0); player1.y = temprand;
                        player4.shot = false;
                    }

                    if (checkCollision(&player4, &player2))
                    {
                        //Play noise
                        play_sample(soundBoat, 255, pan, pitch, FALSE); //sample, volume, pan, frequency, loop
                        //P2 was killed
                        player4.totalKills++; player2.totalDeaths++;
                        //Put P2 at new coordinates.
                        srand((unsigned)time(NULL)+13); int temprand = (int)775 * rand() / (RAND_MAX + 1.0); player2.x = temprand; temprand = (int)575 * rand() / (RAND_MAX + 1.0); player2.y = temprand;
                        player4.shot = false;
                    }

                    if (checkCollision(&player4, &player3))
                    {
                        //Play noise
                        play_sample(soundSquashy, 255, pan, pitch, FALSE); //sample, volume, pan, frequency, loop
                        //P2 was killed
                        player4.totalKills++; player3.totalDeaths++;
                        //Put P2 at new coordinates.
                        srand((unsigned)time(NULL)); int temprand = (int)775 * rand() / (RAND_MAX + 1.0); player3.x = temprand; temprand = (int)575 * rand() / (RAND_MAX + 1.0); player3.y = temprand;
                        player4.shot = false;
                    }

                }
                if (frame > 5.0) { frame = 1.0; }
            }//else (if menu isn't open)

        speed_counter--;
        }//while (speed_counter > 0)


        if (menu == true && helpwindow == true)
        {
            draw_sprite(buffer, imgBackground1, 0, 0);
            displayImage(&windowHelp, buffer);
            displayImage(&btnBack, buffer);

            if (mouseClick == true) { masked_blit (cursor, buffer, (cursor->w / 2), 0, mouse_x, mouse_y, (cursor->w / 2), cursor->h); }
            else { masked_blit (cursor, buffer, 0, 0, mouse_x, mouse_y, (cursor->w / 2), cursor->h); }

        }
        else if (menu == true && helpwindow == false)
        {
            draw_sprite(buffer, imgBackground1, 0, 0);
            draw_sprite(buffer, imgMenuBox, 250, 150);
            draw_sprite(buffer, imgTitle, 260, 160);

            displayImage(&btnPlay, buffer);
            displayImage(&btnHelp, buffer);
            displayImage(&btnExit, buffer);

            if (fullscreen == false) { displayImage(&btnFullscreen, buffer); }
            else { displayImage(&btnWindow, buffer); }

            if (mouseClick == true) { masked_blit (cursor, buffer, (cursor->w / 2), 0, mouse_x, mouse_y, (cursor->w / 2), cursor->h); }
            else { masked_blit (cursor, buffer, 0, 0, mouse_x, mouse_y, (cursor->w / 2), cursor->h); }
        }
        else if (menu == false) //in-game
        {
            //draw_sprite(buffer, imgBackground2, 0, 0);

            if (frame >= 1.0 && frame < 2.0)
            {
                if   (player1.direction == 'r')  { masked_blit(player1.img, buffer, 0, 0,    player1.x, player1.y, player1.spriteWidth, player1.spriteHeight); }
                else                            { masked_blit(player1.img, buffer, 84, 0,   player1.x, player1.y, player1.spriteWidth, player1.spriteHeight); }

                if (player2.direction == 'r')    { masked_blit(player2.img, buffer, 0, 0,    player2.x, player2.y, player2.spriteWidth, player2.spriteHeight); }
                else                            { masked_blit(player2.img, buffer, 64, 0,   player2.x, player2.y, player2.spriteWidth, player2.spriteHeight); }

                if (player3.direction == 'r')    { masked_blit(player3.img, buffer, 0, 0,    player3.x, player3.y, player3.spriteWidth, player3.spriteHeight); }
                else                            { masked_blit(player3.img, buffer, 74, 0,   player3.x, player3.y, player3.spriteWidth, player3.spriteHeight); }

                if (player4.direction == 'r')    { masked_blit(player4.img, buffer, 0, 0,    player4.x, player4.y, player4.spriteWidth, player4.spriteHeight); }
                else                            { masked_blit(player4.img, buffer, 105, 0,  player4.x, player4.y, player4.spriteWidth, player4.spriteHeight); }

            }
            else if ( (frame >= 2.0 && frame < 3.0) || (frame >= 4.0 && frame < 5.0) )
            {
                if (player1.direction == 'r')    { masked_blit(player1.img, buffer, 0+player1.spriteWidth,   0, player1.x, player1.y, player1.spriteWidth, player1.spriteHeight); }
                else                            { masked_blit(player1.img, buffer, 84+player1.spriteWidth,  0, player1.x, player1.y, player1.spriteWidth, player1.spriteHeight); }

                if (player2.direction == 'r')    { masked_blit(player2.img, buffer, 0+player2.spriteWidth,   0, player2.x, player2.y, player2.spriteWidth, player2.spriteHeight); }
                else                            { masked_blit(player2.img, buffer, 64+player2.spriteWidth,  0, player2.x, player2.y, player2.spriteWidth, player2.spriteHeight); }

                if (player3.direction == 'r')    { masked_blit(player3.img, buffer, 0+player3.spriteWidth,   0, player3.x, player3.y, player3.spriteWidth, player3.spriteHeight); }
                else                            { masked_blit(player3.img, buffer, 74+player3.spriteWidth,  0, player3.x, player3.y, player3.spriteWidth, player3.spriteHeight); }

                if (player4.direction == 'r')    { masked_blit(player4.img, buffer, 0+player4.spriteWidth,   0, player4.x, player4.y, player4.spriteWidth, player4.spriteHeight); }
                else                            { masked_blit(player4.img, buffer, 105+player4.spriteWidth, 0, player4.x, player4.y, player4.spriteWidth, player4.spriteHeight); }
            }
            else if (frame >= 3.0 && frame < 4.0)
            {
                if (player1.direction == 'r')    { masked_blit(player1.img, buffer, 0+(player1.spriteWidth * 2),     0, player1.x, player1.y, player1.spriteWidth, player1.spriteHeight); }
                else                            { masked_blit(player1.img, buffer, 84+(player1.spriteWidth * 2),    0, player1.x, player1.y, player1.spriteWidth, player1.spriteHeight); }

                if (player2.direction == 'r')    { masked_blit(player2.img, buffer, 0+(player2.spriteWidth * 2),     0, player2.x, player2.y, player2.spriteWidth, player2.spriteHeight); }
                else                            { masked_blit(player2.img, buffer, 64+(player2.spriteWidth * 2),    0, player2.x, player2.y, player2.spriteWidth, player2.spriteHeight); }

                if (player3.direction == 'r')    { masked_blit(player3.img, buffer, 0+(player3.spriteWidth * 2),     0, player3.x, player3.y, player3.spriteWidth, player3.spriteHeight); }
                else                            { masked_blit(player3.img, buffer, 74+(player3.spriteWidth * 2),    0, player3.x, player3.y, player3.spriteWidth, player3.spriteHeight); }

                if (player4.direction == 'r')    { masked_blit(player4.img, buffer, 0+(player4.spriteWidth * 2),     0, player4.x, player4.y, player4.spriteWidth, player4.spriteHeight); }
                else                            { masked_blit(player4.img, buffer, 105+(player4.spriteWidth * 2),   0, player4.x, player4.y, player4.spriteWidth, player4.spriteHeight); }
            }

            //Display lasershots
            if (player1.shot) { masked_blit(lasers, buffer, 0,  0, player1.shotX, player1.shotY, 25, 10); }
            if (player2.shot) { masked_blit(lasers, buffer, 25, 0, player2.shotX, player2.shotY, 25, 10); }
            if (player3.shot) { masked_blit(lasers, buffer, 50, 0, player3.shotX, player3.shotY, 25, 10); }
            if (player4.shot) { masked_blit(lasers, buffer, 75, 0, player4.shotX, player4.shotY, 25, 10); }

        }

        if (!menu)
        {
            draw_sprite(buffer, imgScoreList, 0, 530);
            draw_sprite(buffer, imgBlackBar, 0, 565);
            textprintf(buffer,font,5,570,makecol(0,255,255), "Kills: %i", player1.totalKills);
            textprintf(buffer,font,5,578,makecol(0,255,255), "Death: %i", player1.totalDeaths);

            textprintf(buffer,font,205,570,makecol(255,0,0), "Kills: %i", player2.totalKills);
            textprintf(buffer,font,205,578,makecol(255,0,0), "Death: %i", player2.totalDeaths);

            textprintf(buffer,font,405,570,makecol(255,255,0), "Kills: %i", player3.totalKills);
            textprintf(buffer,font,405,578,makecol(255,255,0), "Death: %i", player3.totalDeaths);

            textprintf(buffer,font,605,570,makecol(255,150,0), "Kills: %i", player4.totalKills);
            textprintf(buffer,font,605,578,makecol(255,150,0), "Death: %i", player4.totalDeaths);
        }
        acquire_screen();
        blit(buffer, screen, 0, 0, 0, 0, 800, 600);
        clear_bitmap(buffer);
        release_screen();

    }//while (!key[KEY_ESC])
    return 0;

}
END_OF_MAIN();

